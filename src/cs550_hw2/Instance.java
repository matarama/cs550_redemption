package cs550_hw2;

import java.util.Formatter;

public class Instance 
{
	public double[] features;
	public int f;
	public int z;
	
	public Instance()
	{
		this(null, -1);
	}
	
	public Instance(double[] features, int f)
	{
		super();
		this.features = features;
		this.f = f;
		this.z = -1;
	}
	
	public int[] getClassVector(int noOfClasses)
	{
		int[] classVector = new int[noOfClasses];
		classVector[f - 1] = 1;
		return classVector;
	}
	
	public Instance parse(String input)
	{
		String[] fields = input.split(" ");
		features = new double[fields.length - 1];
		
		for(int i = 0; i < fields.length - 1; ++i)
			features[i] = Double.parseDouble(fields[i]);
		
		f = Integer.parseInt(fields[fields.length - 1]);
		
		return this;
	}
	
	public Instance copy()
	{
		Instance copy = new Instance(new double[features.length], f);
		for(int i = 0; i < features.length; ++i)
			copy.features[i] = features[i];
		
		return copy;
	}
	
	public String toString()
	{
		Formatter format = new Formatter();
		for(int i = 0; i < features.length; ++i)
		{
			format.format("%.2f ", features[i]);
		}
		
		return format.toString() + ("f=" + f);
	}
	
	public int getFeatureCount()
	{
		return features.length;
	}
	
	public double getFeature(int i)
	{
		return features[i];
	}
}
