package cs550_hw2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MultiLayerPerceptron 
{
	private double learningRate;
	private int noOfClasses;
	private int maxIterationCount;
	private double convergenceStepThreshold;
	private double trainingAccuracyThreshold;
	private NeuralNetwork nn;
	
	public MultiLayerPerceptron(
		double learningRate, int noOfClasses, int maxIterationCount,
		double convergenceStepThreshold, double trainingAccuracyThreshold, 
		int... unitCountPerLayer)
	{
		this.learningRate = learningRate;
		this.noOfClasses = noOfClasses;
		this.maxIterationCount = maxIterationCount;
		this.convergenceStepThreshold = convergenceStepThreshold;
		this.trainingAccuracyThreshold = trainingAccuracyThreshold;
		this.nn = new NeuralNetwork(unitCountPerLayer);
	}
	
	
	
	public static void kfold(DataSet dataset,
			double learningRate, int noOfClasses, int maxIterationCount, 
			double convergenceStepThreshold, double trainingAccuracyThreshold,
			int inputUnitCount, int hiddenUnitCount, int k)
	{
		List<DataSet> kfold = dataset.getKFold(k);
		
		for(int f = 0; f < k; ++f)
		{
			System.out.println("------------------- Fold (" + (f + 1) + "/" + k + ") -------------------");
			DataSet ts = kfold.remove(f);
			DataSet tr = DataSet.mergeAll(kfold);
			
			System.out.print("Training ");
			tr.printClasses();
			System.out.print("Test ");
			ts.printClasses();
			
			MultiLayerPerceptron mlp = new MultiLayerPerceptron(
					learningRate, noOfClasses, maxIterationCount, convergenceStepThreshold, trainingAccuracyThreshold, 
					inputUnitCount, hiddenUnitCount, noOfClasses);
			
			mlp.gradientDescentStochastic(tr);
			int[][] confusion = mlp.test(ts);
			
			kfold.add(f, ts);
			
			System.out.println("-----------------------------------------------");
		}
	}
	
	public int[][] test(DataSet ts)
	{
		int[][] confusion = new int[noOfClasses][noOfClasses];
		
		for(Instance t : ts.getInstances())
		{
			List<double[]> layerData = forwardPropagation(t);
			double[] z = layerData.get(layerData.size() - 1);
			
			// System.out.println("z: " + Arrays.toString(z));
			
			int maxIndex = 1;
			for(int c = 2; c < noOfClasses + 1; ++c)
				if(z[maxIndex] < z[c])
					maxIndex = c;
			
			confusion[t.f - 1][maxIndex - 1] += 1;
		}
		
		printConfusionMatrix(confusion);
		
		return confusion;
	}
	
	public void gradientDescentStochastic(DataSet dataset)
	{
		double error = 0.0;
		double previousError = 0.0;
		int iterationSoFar = 0;
		int noOfInstances = dataset.getInstanceCount();
		
		do
		{
			error = 0.0;
			++iterationSoFar;
			List<Instance> instanceBuffer = new ArrayList<Instance>();
			
			while(!dataset.isEmpty())
			{
				int r = ((int)(Math.random() * 100000000)) % dataset.getInstanceCount();
				Instance t = dataset.remove(r);
				instanceBuffer.add(t);
				
				List<double[]> layerData = forwardPropagation(t);				
				List<double[][]> deltaValues = backwardPropogation(layerData, t);
				
				nn.updateEdgeWeights(deltaValues);
				
				// calculate error
				double[] zt = layerData.get(layerData.size() - 1);
				
				
				int[] rt = t.getClassVector(noOfClasses);
				for(int c = 0; c < rt.length; ++c)
				{
					error -= rt[c] * Math.log(zt[c + 1]);
				}
			}
			
			error /= noOfInstances;
			// System.out.printf("It:%d error=%.6f\n", iterationSoFar, error);
			
			if(iterationSoFar > maxIterationCount 
				|| Math.abs(error - previousError) < convergenceStepThreshold 
				|| trainingAccuracyThreshold < (100 - error))
				break;
			
			previousError = error;
			dataset.setInstances(instanceBuffer);
		}
		while(true);
		
		System.out.printf("Converged after %d iterations, error=%.6f\n", iterationSoFar, error);
	}
	
	public void gradientDescentBatch(DataSet dataset)
	{
		double error = 0.0;
		double previousError = 0.0;
		int iterationSoFar = 0;
		int noOfInstances = dataset.getInstanceCount();
		
		do
		{
			error = 0.0;
			++iterationSoFar;
			List<Instance> instanceBuffer = new ArrayList<Instance>();
			List<double[][]> deltaValues = createBatchDeltaValues();
			
			while(!dataset.isEmpty())
			{
				int r = ((int)(Math.random() * 100000000)) % dataset.getInstanceCount();
				Instance t = dataset.remove(r);
				instanceBuffer.add(t);
				
				List<double[]> layerData = forwardPropagation(t);
				List<double[][]> deltaUpdates = backwardPropogation(layerData, t);
				
				for(int du = 0; du < deltaValues.size(); ++du)
				{
					double[][] currUpdate = deltaUpdates.get(du);
					double[][] deltaValue = deltaValues.get(du);
					for(int i = 0; i < currUpdate.length; ++i)
						for(int j = 0; j < currUpdate[i].length; ++j)
							deltaValue[i][j] += currUpdate[i][j];			
				}
				
				
				// calculate error
				double[] zt = layerData.get(layerData.size() - 1);
				int[] rt = getClassVector(t);
				for(int c = 0; c < zt.length; ++c)
				{
					error -= rt[c] * Math.log(zt[c]);
				}
			}
			
			nn.updateEdgeWeights(deltaValues);
			error /= noOfInstances;
			System.out.printf("It:%d error=%.3f\n", iterationSoFar, error);
			
			if(iterationSoFar > maxIterationCount 
				|| Math.abs(error - previousError) < convergenceStepThreshold 
				|| trainingAccuracyThreshold < (100 - error))
				break;
			
			previousError = error;
			dataset.setInstances(instanceBuffer);
		}
		while(true);
		
		System.out.printf("Converged after %d iterations, error=%.3f\n", maxIterationCount, error);
	}
	
	// --------------------------------------------------------------------------------------------------------
	
	private List<double[]> forwardPropagation(Instance t)
	{
		List<double[]> dataPerLayer = new ArrayList<double[]>();
		
		// input layer
		double[] x = new double[t.features.length + 1];
		x[0] = 1; // bias
		for(int i = 1; i < x.length; ++i)
			x[i] = t.features[i - 1];
		dataPerLayer.add(x);
		
		// System.out.println("X: " + x.length);
		
		for(int i = 1; i < nn.getNoOfLayers(); ++i)
		{
			double[][] currLayer = nn.getLayer(i - 1);
			double[] prevLayerData = dataPerLayer.get(i - 1);
			double[] nextLayerData = new double[currLayer.length + 1];
			nextLayerData[0] = 1; // bias
		
			
			
			// System.out.println("N: " + nextLayerData.length + " [" + Arrays.toString(nextLayerData) + "]");
			
			// System.out.println("currLayer(" + currLayer.length + ", " + currLayer[0].length + ")");
			
			for(int j = 0; j < currLayer.length; ++j)
			{
				nextLayerData[j + 1] = 0.0;
				for(int k = 0; k < currLayer[j].length; ++k)
				{
					nextLayerData[j + 1] += currLayer[j][k] * prevLayerData[k]; 
				}
			}
			
			// System.out.println("N: " + nextLayerData.length + " [" + Arrays.toString(nextLayerData) + "]");
		
			// use sigmoid function if its a hidden layer
			if(i < nn.getNoOfLayers() - 1)
			{
				sigmoid(nextLayerData);
				nextLayerData[0] = 1;
			}
			// use softmax function to calculate output layer
			else
			{
				softmax(nextLayerData);
			}
			
			dataPerLayer.add(nextLayerData);
		}
		
		return dataPerLayer;
	}
	
	// TODO convert to n layers??
	private List<double[][]> backwardPropogation(List<double[]> layerData, Instance t)
	{
		double[][] W = nn.getLayer(0);
		double[][] V = nn.getLayer(1);
		
		double[] xt = layerData.get(0);
		double[] yt = layerData.get(1);
		double[] zt = layerData.get(2);
		int[] rt = getClassVector(t);
		
//		System.out.println("W => (" + W.length + ", " + W[0].length + ")");
//		System.out.println("V => (" + V.length + ", " + V[0].length + ")");
//		System.out.println("xt => (" + xt.length + ")");
//		System.out.println("yt => (" + yt.length + ")");
//		System.out.println("zt => (" + zt.length + ")");
//		System.out.println("rt => (" + rt.length + ")");
		
		// delta V
		double[][] deltaV = new double[V.length][V[0].length];
		for(int k = 0; k < deltaV.length; ++k)
		{
			for(int j = 0; j < deltaV[k].length; ++j)
			{
				deltaV[k][j] = learningRate * (rt[k] - zt[k + 1]) * yt[j];
			}
		}
		
		// delta W
		double[][] deltaW = new double[W.length][W[0].length];
		for(int j = 0; j < deltaW.length; ++j)
		{
			for(int i = 0; i < deltaW[j].length; ++i)
			{
				for(int c = 0; c < noOfClasses; ++c)
				{
					deltaW[j][i] += (rt[c] - zt[c + 1]) * V[c][j];
				}
				deltaW[j][i] = learningRate * deltaW[j][i] * yt[j + 1] * (1 - yt[j + 1]) * xt[i];
			}
		}
		
		return Arrays.asList(deltaW, deltaV);
	}
	
	private List<double[][]> createBatchDeltaValues()
	{
		List<double[][]> dvs = new ArrayList<double[][]>();
		for(int l = 0; l < nn.getNoOfLayers() - 1; ++l)
		{
			dvs.add(new double[nn.getUnitCount(l + 1) - 1][nn.getUnitCount(l)]);
		}
		
		return dvs;
	}
	
	private int[] getClassVector(Instance t)
	{
		int[] cv = new int[noOfClasses];
		cv[t.f - 1] = 1;
		return cv;
	}
	
	private double sigmoid(double value)
	{
		if(value >= 4)
			return 1;
		if(value <= -4)
			return 0;
		
		return 1.0 / (1.0 + Math.exp(-value));
	}
	
	private void sigmoid(double[] values)
	{
		for(int i = 0; i < values.length; ++i)
			values[i] = sigmoid(values[i]);
	}
	
	private void softmax(double[] values)
	{
		double denominator = 0.0;
		for(int i = 0; i < values.length; ++i)
		{
			values[i] = Math.exp(values[i]);
			denominator += values[i];
		}
		
		for(int i = 0; i < values.length; ++i)
			values[i] = values[i] / denominator;
	}
	
	private void printConfusionMatrix(int[][] c)
	{
		int[] perc = new int[noOfClasses];
		for(int i = 0; i < c.length; ++i)
			for(int j = 0; j < c[i].length; ++j)
				perc[i] += c[i][j];
		
		System.out.println("Act\tPredicted");
		System.out.println("\tC1\tC2\tC3");
		for(int i = 0; i < c.length; ++i)
		{
			System.out.print("C" + (i + 1) + ":\t");
			for(int j = 0; j < c.length; ++j)
			{
				if(i == j)
				{
					System.out.printf("%d(%.0f)\t", c[i][j], ((double) c[i][j] / perc[i]) * 100);
				}
				else
				{
					System.out.print(c[i][j] + "\t");
				}
			}
			System.out.println();
		}
	}
}
