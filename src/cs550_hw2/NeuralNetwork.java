package cs550_hw2;

import java.util.Formatter;
import java.util.List;

public class NeuralNetwork 
{
	private int noOfLayers;
	private int[] unitCountEachLayer;
	private double[][][] layers;

	public NeuralNetwork(int... unitCountPerLayer)
	{
		this.noOfLayers = unitCountPerLayer.length;
		this.unitCountEachLayer = new int[noOfLayers];
		for(int i = 0; i < noOfLayers - 1; ++i)
			unitCountEachLayer[i] = unitCountPerLayer[i] + 1; // bias term
		unitCountEachLayer[noOfLayers - 1] = unitCountPerLayer[noOfLayers - 1];
		
		this.layers = new double[noOfLayers - 1][][];
		this.create();
	}

	public void updateEdgeWeights(List<double[][]> deltaValues)
	{
		for(int i = 0; i < deltaValues.size(); ++i)
		{
			double[][] layer = layers[i];
			double[][] delta = deltaValues.get(i);
			
			for(int k = 0; k < layer.length; ++k)
				for(int l = 0; l < layer[k].length; ++l)
					layer[k][l] += delta[k][l];
		}
	}
	
	public void create()
	{
		for(int i = 0; i < noOfLayers; ++i)
			assert unitCountEachLayer[i] != 0;
		
		for(int i = 0; i < noOfLayers - 1; ++i)
		{
			int unitCountCurrentLayer = unitCountEachLayer[i];
			int unitCountNextLayer = unitCountEachLayer[i + 1];
			int rows = unitCountNextLayer - 1;
			if(i == noOfLayers - 2)
				++rows;
				
			double[][] layer = new double[rows][unitCountCurrentLayer];
			
			for(int j = 0; j < rows; ++j)
			{
				for(int k = 0; k < unitCountCurrentLayer; ++k)
				{
					// default weight from layer i to j 
					layer[j][k] = ((Math.random() * 10000000) % 0.9) + 0.1;
				}
			}
			
			layers[i] = layer;
		}
	}
	
	public String toString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append("Neural Network (" + noOfLayers + " layers)\n");
		Formatter f = new Formatter();
		
		for(int i = 0; i < noOfLayers - 1; ++i)
		{
			double[][] layer = layers[i];
			f.format("Layer " + i + " -> " + (i + 1) + " (" + layer[0].length + ", " + layer.length + ")" + "\n");
			
			for(int j = 0; j < layer.length; ++j)
			{
				for(int k = 0; k < layer[j].length; ++k)
				{
					f.format("%.3f ", layer[j][k]);
				}
				f.format("\n");
			}
			
			f.format("\n");
		}
		
		return f.toString();
	}

	public double[][] getLayer(int index)
	{
		return layers[index];
	}
	
	public int getUnitCount(int layerNo)
	{
		return unitCountEachLayer[layerNo];
	}
	
	// Getter & Setter
	// -------------------------------------------------------------------------
	
	public int getNoOfLayers() {
		return noOfLayers;
	}

	public void setNoOfLayers(int noOfLayers) {
		this.noOfLayers = noOfLayers;
	}

	public int[] getUnitCountEachLayer() {
		return unitCountEachLayer;
	}

	public void setUnitCountEachLayer(int[] unitCountEachLayer) {
		this.unitCountEachLayer = unitCountEachLayer;
	}

	public double[][][] getLayers() {
		return layers;
	}

	public void setLayers(double[][][] layers) {
		this.layers = layers;
	}
	
	
}
