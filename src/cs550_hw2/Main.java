package cs550_hw2;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Random;

public class Main
{
	public static void main(String[] args)
	{
		DataSet tr = null;
		DataSet ts = null;
		try 
		{
			tr = DataSet.readThyroid("ann-train.data");
			ts = DataSet.readThyroid("ann-test.data");
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		
		// Over-sample training instances
		DataSet oversampled = tr.oversample(500);
		oversampled.normalizeMinMax();
		oversampled.printClasses();
		
		double learningRate = 0.006;
		int noOfClasses = 3;
		int maxIterationCount = 200;
		double convergenceStepThreshold = 0.0000001;
		double trainingAccuracyThreshold = 100;
		int inputUnitCount = oversampled.getFeatureCount();
		int hiddenUnitCount = 50;
		int k = 5;
		
//		MultiLayerPerceptron.kfold(
//				oversampled, learningRate, noOfClasses, maxIterationCount, convergenceStepThreshold, 
//				trainingAccuracyThreshold, inputUnitCount, hiddenUnitCount, k);
//		
//		System.out.println("==================================================");
//		System.out.print("Training "); tr.printClasses();
//		System.out.print("Test "); ts.printClasses();
		
		MultiLayerPerceptron mlp = new MultiLayerPerceptron(
				learningRate, noOfClasses, maxIterationCount, convergenceStepThreshold, 
				trainingAccuracyThreshold, inputUnitCount, hiddenUnitCount, noOfClasses);
		mlp.gradientDescentStochastic(oversampled);
		mlp.test(ts);
	}
}
