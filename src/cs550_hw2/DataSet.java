package cs550_hw2;

import java.beans.FeatureDescriptor;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.crypto.spec.DESedeKeySpec;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

public class DataSet 
{
	private List<Instance> instances;
	private int noOfClasses;
	
	public DataSet(int noOfClasses)
	{
		this(new ArrayList<Instance>(), noOfClasses);
	}
	
	public DataSet(List<Instance> instances, int noOfClasses)
	{
		super();
		this.instances = instances;
		this.noOfClasses = noOfClasses;
	}
	
	public List<DataSet> getKFold(int k)
	{
		List<DataSet> kfold = new ArrayList<DataSet>();
		
		int sampleSizePerFold = instances.size() / k;
		for(int i = 0; i < k - 1; ++i)
		{
			DataSet fold = new DataSet(noOfClasses);
			for(int t = 0; t < sampleSizePerFold; ++t)
			{
				int r = ((int)(Math.random() * 10000000)) % getInstanceCount();
				fold.addInstance(instances.remove(r));
			}
			kfold.add(fold);
		}
		
		// last fold
		kfold.add(this);
		
		return kfold;
	}
	
	public DataSet subsample(int sampleSize)
	{
		List<List<Instance>> classInstances = getClassInstances();
		List<List<Instance>> subSampled = new ArrayList<List<Instance>>();
		for(int c = 0; c < noOfClasses; ++c)
		{
			List<Instance> allInstances = classInstances.get(c);
			if(allInstances.size() <= sampleSize)
			{
				subSampled.add(allInstances);
				continue;
			}
			
			List<Instance> newInstances = new ArrayList<Instance>();
			for(int i = 0; i < sampleSize; ++i)
			{
				int r = ((int)(Math.random() * 10000000)) % allInstances.size();
				newInstances.add(allInstances.remove(r));
			}
			subSampled.add(newInstances);
		}
		
		return shuffle(subSampled);
	}
	
	public DataSet oversample(int sampleSize)
	{
		Random rand = new Random();
		List<List<Instance>> classInstances = getClassInstances();
		List<List<Instance>> oversampled = new ArrayList<List<Instance>>();
		for(int c = 0; c < noOfClasses; ++c)
		{
			List<Instance> allInstances = classInstances.get(c);
			if(allInstances.size() >= sampleSize)
			{
				oversampled.add(allInstances);
				continue;
			}
			
			List<Instance> newInstances = new ArrayList<Instance>();
			newInstances.addAll(allInstances);
			int dubCount = sampleSize - allInstances.size();
			for(int i = 0; i < dubCount; ++i)
			{
				int r = ((int)(Math.random() * 10000000)) % allInstances.size();
				
				// Add d dimensional gaussian noise
				Instance chosen = allInstances.get(r).copy();
//				for(int f = 0; f < chosen.getFeatureCount(); ++f)
//					if(f == 0 || f == 16 || f == 17 || f == 18 || f == 19 || f == 20)
//						chosen.features[f] += rand.nextGaussian() / 100;
				
				newInstances.add(chosen);
			}
			oversampled.add(newInstances);
		}
		
		return shuffle(oversampled);
	}

	public void normalizeMinMax()
	{
		double[] denominator = new double[getFeatureCount()];
		double[] maxs = new double[getFeatureCount()];
		double[] mins = new double[getFeatureCount()];
		for(int i = 0; i < getFeatureCount(); ++i)
			mins[i] = instances.get(0).features[i];
		
		for(Instance instance : instances)
		{
			for(int i = 0; i < getFeatureCount(); ++i)
			{
				if(mins[i] > instance.getFeature(i))
					mins[i] = instance.getFeature(i);
				else if(maxs[i] < instance.getFeature(i))
					maxs[i] = instance.getFeature(i);
			}
		}
		
		for(int i = 0; i < getFeatureCount(); ++i)
			denominator[i] = maxs[i] - mins[i];
		
		for(Instance instance : instances)
		{
			for(int i = 0; i < getFeatureCount(); ++i)
			{
				instance.features[i] = (instance.features[i] - mins[i]) / denominator[i];
			}
		}
	}	
	
	// -----------------------------------------------------------------------------

	public static DataSet mergeAll(List<DataSet> datasets)
	{
		DataSet merged = new DataSet(datasets.get(0).getNoOfClasses());
		for(DataSet set : datasets)
		{
			DataSet cpy = set.copy();
			merged.addInstance(cpy.instances);
		}
		
		return merged;
	}
	
	public static DataSet readThyroid(String path) throws FileNotFoundException, IOException
	{
		DataSet ds = new DataSet(3);
		
		BufferedReader reader = new BufferedReader(new FileReader(path));
		
		String line = reader.readLine();
		while(line != null)
		{
			ds.addInstance(new Instance().parse(line));
			line = reader.readLine();
		}
		
		reader.close();
		
		return ds;
	}
	
	// -----------------------------------------------------------------------------
	
	private DataSet shuffle(List<List<Instance>> classInstances)
	{
		DataSet d = new DataSet(noOfClasses);
		int sampleSize = 0;
		for(int c = 0; c < noOfClasses; ++c)
			sampleSize += classInstances.get(c).size();
		
		for(int i = 0; i < sampleSize; ++i)
		{
			int rc = ((int) (Math.random() * 1000000)) % classInstances.size();
			
			List<Instance> chosen = classInstances.get(rc);
			while(chosen.isEmpty())
			{
				classInstances.remove(rc);
				rc = ((int) (Math.random() * 1000000)) % classInstances.size();
				chosen = classInstances.get(rc);
			}
				
			int r = ((int) (Math.random() * 1000000)) % chosen.size();
			d.addInstance(chosen.remove(r));
		}
		
		return d;
	}
	
	// -----------------------------------------------------------------------------
	
	public boolean isEmpty()
	{
		return instances.isEmpty();
	}
	
	public Instance remove(int i)
	{
		return instances.remove(i);
	}
	
	public int[] getClassCount()
	{
		int[] cc = new int[noOfClasses];
		for(Instance t : instances)
		{
			++cc[t.f - 1];
		}
		
		return cc;
	}
	
	public List<List<Instance>> getClassInstances() 
	{
		List<List<Instance>> classInstances = new ArrayList<List<Instance>>();
		for(int c = 0; c < noOfClasses; ++c)
			classInstances.add(new ArrayList<Instance>());
		
		for(Instance t : instances)
		{
			classInstances.get(t.f - 1).add(t);
		}
		
		return classInstances;
	}
	
	public void addInstance(Instance t)
	{
		instances.add(t);
	}
	
	public void addInstance(List<Instance> instances)
	{
		this.instances.addAll(instances);
	}
	
	public DataSet copy()
	{
		DataSet copy = new DataSet(noOfClasses);
		for(Instance t : instances)
			copy.addInstance(t.copy());
		
		return copy;
	}
	
	public void printClasses()
	{
		int[] cc = getClassCount();
		System.out.println("Data Set: " + instances.size() + " instances.");
		
		for(int i = 0; i < cc.length; ++i)
		{
			System.out.println("C" + (i + 1) + "=" + cc[i]);
		}
	}

	public List<DescriptiveStatistics> printStats()
	{
		printClasses();
		
		List<DescriptiveStatistics> stats = new ArrayList<DescriptiveStatistics>();
		for(int i = 0; i < getFeatureCount(); ++i)
			stats.add(new DescriptiveStatistics());
		
		for(Instance t : instances)
		{
			for(int i = 0; i < getFeatureCount(); ++i)
			{
				stats.get(i).addValue(t.getFeature(i));
			}
		}
		
		System.out.println("F\tMax\tMin\tStd\tMean\tGeo");
		for(int i = 0; i < getFeatureCount(); ++i)
		{
			System.out.print("f" + i + ":\t");
			System.out.printf("%.3f\t", stats.get(i).getMax());
			System.out.printf("%.3f\t", stats.get(i).getMin());
			System.out.printf("%.3f\t", stats.get(i).getStandardDeviation());
			System.out.printf("%.3f\t", stats.get(i).getMean());
			System.out.printf("%.3f\t", stats.get(i).getGeometricMean());
			System.out.println();
		}
		
		return stats;
	}
	
	public void print()
	{
		printStats();
		for(Instance t : instances)
		{
			System.out.println(t);
		}
	}

	// Getter & Setter
	// -----------------------------------------------------------------------------
	
	public int getFeatureCount()
	{
		return instances.get(0).getFeatureCount();
	}
	
	public int getInstanceCount()
	{
		return instances.size();
	}
	
	public List<Instance> getInstances() 
	{
		return instances;
	}

	public void setInstances(List<Instance> instances) 
	{
		this.instances = instances;
	}

	public int getNoOfClasses() 
	{
		return noOfClasses;
	}

	public void setNoOfClasses(int noOfClasses) 
	{
		this.noOfClasses = noOfClasses;
	}
}
